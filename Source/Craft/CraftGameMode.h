#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CraftGameMode.generated.h"

UCLASS(minimalapi)
class ACraftGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACraftGameMode();
};



