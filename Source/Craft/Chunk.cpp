#include "Chunk.h"
#include "BlockInfo.h"
#include "SimplexNoise.h"
#include "ProceduralMeshComponent.h"

enum class ETerrainSurface {
	Grass,
	Sand
};

struct TerrainGenerator {
	SimplexNoise Noise;

	ETerrainSurface GetSurface(int32 X, int32 Y)
	{
		return Noise.fractal(10, X / 50.f / 4, Y / 50.f / 4) > 0 ? ETerrainSurface::Grass : ETerrainSurface::Sand;
	}

	float GetHeight(int32 X, int32 Y)
	{
		return (Noise.fractal(10, 10 * X / 1024.f / 4.f, 10 * Y / 1024.f / 4.f) + 1) / 2.0f;
	}
};

void FBlockMesh::Clear()
{
	Vertices.Reset();
	Triangles.Reset();
	Normals.Reset();
	UV.Reset();
}

void FBlockMesh::Reserve()
{
	Vertices.Reserve(98304);
	Normals.Reserve(98304);
	UV.Reserve(98304);
	Triangles.Reserve(147456);
}

const int32 AChunk::CHUNK_WIDTH = FSection::SECTION_WIDTH;
const int32 AChunk::CHUNK_LENGTH = FSection::SECTION_LENGTH;
const int32 AChunk::CHUNK_HEIGHT = FSection::SECTION_HEIGHT * CHUNK_SECTIONS;
const int32 AChunk::CHUNK_RAW_SIZE = CHUNK_WIDTH * CHUNK_LENGTH * CHUNK_HEIGHT * sizeof(FBlock);

// Sets default values
AChunk::AChunk()
	: Terrain(nullptr)
{
	PrimaryActorTick.bCanEverTick = true;

	PMC = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("Root"));
	PMC->bUseAsyncCooking = true;
	RootComponent = PMC;

	Material = Cast<UMaterial>(StaticLoadObject(UMaterialInterface::StaticClass(), NULL, TEXT("/Game/Materials/M_Block")));
}

FBlock AChunk::GetBlock(int32 W, int32 L, int32 H) const
{
	check(W >= 0);
	check(L >= 0);
	check(H >= 0);
	check(W < CHUNK_WIDTH);
	check(L < CHUNK_LENGTH);
	check(H < CHUNK_HEIGHT);

	const int32 Section = H / FSection::SECTION_HEIGHT;
	const int32 Layer = H % FSection::SECTION_HEIGHT;
	return Sections[Section].Blocks[Layer][L][W];
}

FIntPoint AChunk::GetChunkId() const
{
	return ChunkId;
}

void AChunk::SetBlock(int32 W, int32 L, int32 H, FBlock Block)
{
	check(W >= 0);
	check(L >= 0);
	check(H >= 0);
	check(W < CHUNK_WIDTH);
	check(L < CHUNK_LENGTH);
	check(H < CHUNK_HEIGHT);

	const int32 Section = H / FSection::SECTION_HEIGHT;
	const int32 Layer = H % FSection::SECTION_HEIGHT;
	Sections[Section].Blocks[Layer][L][W] = Block;
}

#if WITH_EDITOR
void AChunk::OnConstruction(const FTransform& Transform)
{
	GenerateBlocks();
	UpdateMesh();
}
#endif

// Called every frame
void AChunk::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AChunk::SetBlock(uint32 X, uint32 Y, uint32 Z, FBlock Block)
{
	const uint32 S = Z / FSection::SECTION_HEIGHT;
	check(S < CHUNK_SECTIONS);

	Sections[S].Blocks[Z % FSection::SECTION_HEIGHT][Y][X] = Block;
}

void AChunk::GenerateBlocks()
{
	TerrainGenerator Gen;

	for (int32 l = 0; l < FSection::SECTION_LENGTH; ++l) {
		for (int32 w = 0; w < FSection::SECTION_WIDTH; ++w) {
			const float x = float(ChunkId.X * FSection::SECTION_WIDTH + w);
			const float y = float(ChunkId.Y * FSection::SECTION_LENGTH + l);

			const auto Surface = Gen.GetSurface(x, y);
			const int32 BlockHeight = 10 + Gen.GetHeight(x, y) * Gen.GetHeight(x, y) * 50;

			for (int32 h = 0; h < CHUNK_BLOCK_SIZE; ++h) {
				FBlock B = BLOCK_AIR;

				if ((h < BlockHeight)) {
					if (h < 5) {
						B = BLOCK_STONE;
					} else {
						if (Surface == ETerrainSurface::Grass) {
							if (h == BlockHeight - 1) {
								B = h < 40 ? BLOCK_GRASS : BLOCK_SNOW;
							}
							else {
								B = BLOCK_DIRT;
							}
						}
						else { // sand
							B = h < 30 ? BLOCK_SAND : BLOCK_STONE;
						}
					}
				}

				SetBlock(w, l, h, B);
			}
		}
	}
}

void AChunk::UpdateMesh()
{
	FBlockMesh SectionMesh;
	SectionMesh.Reserve();

	for (int32 i = 0; i < CHUNK_SECTIONS; ++i) {
		if (Material != nullptr) {
			PMC->SetMaterial(i, Material);
		}

		const float SectionHeight = i * FSection::SECTION_HEIGHT;
		const FSection& Section = Sections[i];
		
		for (int32 h = 0; h < FSection::SECTION_HEIGHT; ++h) {
			for (int32 l = 0; l < FSection::SECTION_LENGTH; ++l) {
				for (int32 w = 0; w < FSection::SECTION_WIDTH; ++w) {
					FBlock Block = Section.Blocks[h][l][w];

					if (Block.Type == BLOCK_AIR.Type) {
						continue;
					}

					EBlockFace NeighbourFaces = Section.GetVisibleFaces(w, l, h);

					const auto& BlockInfo = GetBlockInfo(Block.Type);
					AddBlockMesh(w, l, h + SectionHeight, BlockInfo, NeighbourFaces, SectionMesh);
				}
			}
		}
		
		PMC->CreateMeshSection(i, SectionMesh.Vertices, SectionMesh.Triangles, SectionMesh.Normals, SectionMesh.UV, {}, {}, true);

		SectionMesh.Clear();
	}
}

void AChunk::AddBlockMesh(int32 w, int32 l, int32 h, const FBlockInfo& BlockInfo, EBlockFace Faces, FBlockMesh& Mesh)
{
	if (Faces & BF_Front) {
		const int32 TriangleIdx = Mesh.Vertices.Num();
		Mesh.Vertices.Add(FVector(w * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Front[0].X, BlockInfo.Faces.Front[1].Y });
		Mesh.Normals.Add({ 0, 1, 0 });
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Front[1].X, BlockInfo.Faces.Front[1].Y });
		Mesh.Normals.Add({ 0, 1, 0 });
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Front[1].X, BlockInfo.Faces.Front[0].Y });
		Mesh.Normals.Add({ 0, 1, 0 });
		Mesh.Vertices.Add(FVector(w * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Front[0].X, BlockInfo.Faces.Front[0].Y });
		Mesh.Normals.Add({ 0, 1, 0 });

		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 1);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 3);
	}

	if (Faces & BF_Back) {
		const int32 TriangleIdx = Mesh.Vertices.Num();
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, (l)*FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Back[0].X, BlockInfo.Faces.Back[1].Y });
		Mesh.Normals.Add({ 0, -1, 0 });
		Mesh.Vertices.Add(FVector((w)*FBlock::WORLD_SIZE, (l)*FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Back[1].X, BlockInfo.Faces.Back[1].Y });
		Mesh.Normals.Add({ 0, -1, 0 });
		Mesh.Vertices.Add(FVector(w * FBlock::WORLD_SIZE, (l)*FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Back[1].X, BlockInfo.Faces.Back[0].Y });
		Mesh.Normals.Add({ 0, -1, 0 });
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, (l)*FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Back[0].X, BlockInfo.Faces.Back[0].Y });
		Mesh.Normals.Add({ 0, -1, 0 });

		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 1);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 3);
	}

	if (Faces & BF_Left) {
		const int32 TriangleIdx = Mesh.Vertices.Num();
		Mesh.Vertices.Add(FVector(w * FBlock::WORLD_SIZE, l * FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Left[0].X, BlockInfo.Faces.Left[1].Y });
		Mesh.Normals.Add({ -1, 0, 0 });
		Mesh.Vertices.Add(FVector(w * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Left[1].X, BlockInfo.Faces.Left[1].Y });
		Mesh.Normals.Add({ -1, 0, 0 });
		Mesh.Vertices.Add(FVector(w * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Left[1].X, BlockInfo.Faces.Left[0].Y });
		Mesh.Normals.Add({ -1, 0, 0 });
		Mesh.Vertices.Add(FVector(w * FBlock::WORLD_SIZE, l * FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Left[0].X, BlockInfo.Faces.Left[0].Y });
		Mesh.Normals.Add({ -1, 0, 0 });

		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 1);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 3);
	}

	if (Faces & BF_Right) {
		const int32 TriangleIdx = Mesh.Vertices.Num();
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Right[0].X, BlockInfo.Faces.Right[1].Y });
		Mesh.Normals.Add({ 1, 0, 0 });
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, l * FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Right[1].X, BlockInfo.Faces.Right[1].Y });
		Mesh.Normals.Add({ 1, 0, 0 });
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, l * FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Right[1].X, BlockInfo.Faces.Right[0].Y });
		Mesh.Normals.Add({ 1, 0, 0 });
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Right[0].X, BlockInfo.Faces.Right[0].Y });
		Mesh.Normals.Add({ 1, 0, 0 });

		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 1);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 3);
	}

	if (Faces & BF_Top) {
		const int32 TriangleIdx = Mesh.Vertices.Num();
		Mesh.Vertices.Add(FVector(w * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Top[0].X, BlockInfo.Faces.Top[1].Y });
		Mesh.Normals.Add({ 0, 0, 1 });
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Top[1].X, BlockInfo.Faces.Top[1].Y });
		Mesh.Normals.Add({ 0, 0, 1 });
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, l * FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Top[1].X, BlockInfo.Faces.Top[0].Y });
		Mesh.Normals.Add({ 0, 0, 1 });
		Mesh.Vertices.Add(FVector(w * FBlock::WORLD_SIZE, l * FBlock::WORLD_SIZE, (h + 1) * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Top[0].X, BlockInfo.Faces.Top[0].Y });
		Mesh.Normals.Add({ 0, 0, 1 });

		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 1);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 3);
	}

	if (Faces & BF_Bottom) {
		const int32 TriangleIdx = Mesh.Vertices.Num();
		Mesh.Vertices.Add(FVector(w * FBlock::WORLD_SIZE, l * FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Bottom[0].X, BlockInfo.Faces.Bottom[1].Y });
		Mesh.Normals.Add({ 0, 0, -1 });
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, l * FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Bottom[1].X, BlockInfo.Faces.Bottom[1].Y });
		Mesh.Normals.Add({ 0, 0, -1 });
		Mesh.Vertices.Add(FVector((w + 1) * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Bottom[1].X, BlockInfo.Faces.Bottom[0].Y });
		Mesh.Normals.Add({ 0, 0, -1 });
		Mesh.Vertices.Add(FVector(w * FBlock::WORLD_SIZE, (l + 1) * FBlock::WORLD_SIZE, h * FBlock::WORLD_SIZE));
		Mesh.UV.Add({ BlockInfo.Faces.Bottom[0].X, BlockInfo.Faces.Bottom[0].Y });
		Mesh.Normals.Add({ 0, 0, -1 });

		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 1);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 0);
		Mesh.Triangles.Add(TriangleIdx + 2);
		Mesh.Triangles.Add(TriangleIdx + 3);
	}
}

const FBlockInfo& AChunk::GetBlockInfo(uint32 BlockType)
{
	return BlockInfoTable[BlockType];
}

