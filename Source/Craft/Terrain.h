#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Terrain.generated.h"

class AChunk;
struct FBlock;

UCLASS()
class CRAFT_API ATerrain : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	int32 WidthChunks = 17;

	UPROPERTY(EditAnywhere)
	int32 LengthChunks = 17;

	UPROPERTY(EditAnywhere)
	int32 NumCreateMissingChunksPerTick = 1;

	UPROPERTY()
	TMap<FIntPoint, AChunk*> ActiveChunks;

	UPROPERTY()
	TArray<AChunk*> OldChunks;

	TArray<FIntPoint> MissingChunks;

public:	
	static const int32 CHUNK_TERRAIN_SIZE = 1024;	/// terrain width and length is 1024 chunks
	static const FVector WORLD_CENTER_OFFSET;

	ATerrain();

	FBlock GetBlockAt(const FIntVector& BlockId) const;

	bool SetBlockAt(const FIntVector& BlockId, FBlock Block, bool UpdateMesh = true);

	void Tick(float DeltaTime) override;

	static FIntPoint LocationToChunkId(float X, float Y);

	static FIntVector LocationToBlockId(const FVector& Location);

protected:
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	TArray<FIntPoint> GetChunkIdsAround(const FIntPoint& CenterChunkId, int32 Width, int32 Hlength);

	void CreateMissingChunks();

	FIntPoint GetPlayerChunkId() const;

	// Chunk must have set ChunkId
	void LoadOrGenerateChunkAsync(AChunk* Chunk);

	AChunk* NewChunkAt(const FIntPoint& ChunkId);

	void SaveChunkAsync(AChunk* Chunk);

	void UpdateChunks();

	static FVector ChunkIdToLocation(const FIntPoint& ChunkId);

	static FString GetChunkFilename(AChunk* Chunk);
};