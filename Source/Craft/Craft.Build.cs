// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Craft : ModuleRules
{
	public Craft(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });

		PublicIncludePathModuleNames.AddRange(
			new string[] {
				"ProceduralMeshComponent",
			});
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"ProceduralMeshComponent",
			});
	}
}
