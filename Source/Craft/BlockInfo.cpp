#include "BlockInfo.h"

using BlockFaceUVs = FVector[2];

FBlockUV BlockFaces(const FIntPoint& Front, const FIntPoint& Back, const FIntPoint& Left,
	const FIntPoint& Right, const FIntPoint& Top, const FIntPoint& Bottom)
{
	const float W = 16;
	const float H = 16;

	return {
		{{Front.X / W, Front.Y / H}, {(Front.X + 1) / W, (Front.Y + 1) / H}},
		{{Back.X / W, Back.Y / H}, {(Back.X + 1) / W, (Back.Y + 1) / H}},
		{{Left.X / W, Left.Y / H}, {(Left.X + 1) / W, (Left.Y + 1) / H} },
		{{Right.X / W, Right.Y / H}, {(Right.X + 1) / W, (Right.Y + 1) / H}},
		{{Top.X / W, Top.Y / H}, {(Top.X + 1) / W, (Top.Y + 1) / H} },
		{{Bottom.X / W, Bottom.Y / H}, {(Bottom.X + 1) / W, (Bottom.Y + 1) / H}}
	};
}

const FBlockInfo BlockInfoTable[BLOCK_TYPES_COUNT] = {
	{ TEXT("Air") , {}, 0 },
	{ TEXT("Dirt"), BlockFaces({6, 5}, {6, 5}, {6, 5}, {6, 5}, {6, 5}, {6, 5}), 2 },
	{ TEXT("Grass"), BlockFaces({0, 4}, {0, 4}, {0, 4}, {0, 4}, {0, 3}, {0, 5}), 2 },
	{ TEXT("Stone"), BlockFaces({5, 5}, {5, 5}, {5, 5}, {5, 5}, {5, 5}, {5, 5}), 6 },
	{ TEXT("Snow"), BlockFaces({8, 4}, {8, 4}, {8, 4}, {8, 4}, {8, 3}, {8, 5}), 3 },
	{ TEXT("Sand"), BlockFaces({1, 5}, {1, 5}, {1, 5}, {1, 5}, {1, 5}, {1, 5}), 1 },
	{ TEXT("Log"), BlockFaces({4, 4}, {4, 4}, {4, 4}, {4, 4}, {4, 3}, {4, 5}), 3 },
	{ TEXT("Leaves"), BlockFaces({14, 5}, {14, 5}, {14, 5}, {14, 5}, {14, 5}, {14, 5}), 1 },
};

const FBlock BLOCK_AIR = MakeBlock(0);
const FBlock BLOCK_DIRT = MakeBlock(1);
const FBlock BLOCK_GRASS = MakeBlock(2);
const FBlock BLOCK_STONE = MakeBlock(3);
const FBlock BLOCK_SNOW = MakeBlock(4);
const FBlock BLOCK_SAND = MakeBlock(5);


FBlock MakeBlock(int32 BlockType)
{
	check(BlockType < BLOCK_TYPES_COUNT);

	FBlock Block;
	Block.Type = BlockType;
	Block.Health = BlockInfoTable[BlockType].Health;
	return Block;
}


