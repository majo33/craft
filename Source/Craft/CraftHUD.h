#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "CraftHUD.generated.h"

UCLASS()
class ACraftHUD : public AHUD
{
	GENERATED_BODY()

public:
	ACraftHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;
	class UTexture2D* BlocksTexture;

};

