#pragma once

#include "CoreMinimal.h"
#include "Block.h"
#include "Section.generated.h"

enum EBlockFace {
	BF_None		= 0b000000,
	BF_Front	= 0b000001,
	BF_Back		= 0b000010,
	BF_Left		= 0b000100,
	BF_Right	= 0b001000,
	BF_Top		= 0b010000,
	BF_Bottom	= 0b100000,
	BF_All		= 0b111111,
};

USTRUCT()
struct CRAFT_API FSection
{
	GENERATED_BODY()

	static const int32 SECTION_WIDTH = 16;	// X
	static const int32 SECTION_LENGTH = 16;	// Y
	static const int32 SECTION_HEIGHT = 16;	// Z

	static const float SECTION_WORLD_WIDTH;
	static const float SECTION_WORLD_LENGTH;
	static const float SECTION_WORLD_HEIGHT;

	static const float SECTION_HALF_WORLD_WIDTH;
	static const float SECTION_HALF_WORLD_LENGTH;
	static const float SECTION_HALF_WORLD_HEIGHT;

	FBlock Blocks[SECTION_HEIGHT][SECTION_LENGTH][SECTION_WIDTH] = {};

	EBlockFace GetVisibleFaces(uint32 W, uint32 L, uint32 H) const;

	bool IsBlockAtAir(uint32 W, uint32 L, uint32 H) const;
};
