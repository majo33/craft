#pragma once

#include "CoreMinimal.h"
#include "Block.h"
#include "BlockInfo.generated.h"

USTRUCT()
struct FBlockUV {
	GENERATED_BODY()

	UPROPERTY()
	FVector2D Front[2];

	UPROPERTY()
	FVector2D Back[2];

	UPROPERTY()
	FVector2D Left[2];

	UPROPERTY()
	FVector2D Right[2];

	UPROPERTY()
	FVector2D Top[2];

	UPROPERTY()
	FVector2D Bottom[2];
};

/**
 * 
 */
USTRUCT()
struct CRAFT_API FBlockInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	FString Name;

	UPROPERTY(EditAnywhere)
	FBlockUV Faces;

	UPROPERTY(EditAnywhere)
	int32 Health = 10;
};

FBlock MakeBlock(int32 BlockType);

const int32 BLOCK_TYPES_COUNT = 8;
extern const FBlockInfo BlockInfoTable[BLOCK_TYPES_COUNT];
extern const FBlock BLOCK_AIR;
extern const FBlock BLOCK_GRASS;
extern const FBlock BLOCK_STONE;
extern const FBlock BLOCK_SNOW;
extern const FBlock BLOCK_SAND;
extern const FBlock BLOCK_DIRT;