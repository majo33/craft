#include "Terrain.h"
#include "Section.h"
#include "Chunk.h"
#include "BlockInfo.h"
#include "GameFramework/Character.h"

const FVector ATerrain::WORLD_CENTER_OFFSET = { CHUNK_TERRAIN_SIZE * FSection::SECTION_WORLD_WIDTH / 2, CHUNK_TERRAIN_SIZE * FSection::SECTION_WORLD_LENGTH / 2, 0 };
const FString SAVE_DIRECTORY = "Save";

// Sets default values
ATerrain::ATerrain()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ATerrain::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	// save all active chunks
	for (auto ActiveChunk : ActiveChunks) {
		SaveChunkAsync(ActiveChunk.Value);
	}
}

FBlock ATerrain::GetBlockAt(const FIntVector& BlockId) const
{
	const FIntPoint ChunkId = { BlockId.X / AChunk::CHUNK_WIDTH, BlockId.Y / AChunk::CHUNK_LENGTH };
	auto Chunk = ActiveChunks.Find(ChunkId);

	if (Chunk == nullptr) {
		return BLOCK_AIR;
	}

	const int32 W = BlockId.X % AChunk::CHUNK_WIDTH;
	const int32 L = BlockId.Y % AChunk::CHUNK_LENGTH;
	const int32 H = BlockId.Z;
	return (*Chunk)->GetBlock(W, L, H);
}

bool ATerrain::SetBlockAt(const FIntVector& BlockId, FBlock Block, bool UpdateMesh)
{
	check(BlockId.X >= 0);
	check(BlockId.Y >= 0);
	check(BlockId.Z >= 0);
	check(BlockId.X < CHUNK_TERRAIN_SIZE * AChunk::CHUNK_WIDTH);
	check(BlockId.Y < CHUNK_TERRAIN_SIZE * AChunk::CHUNK_LENGTH);
	check(BlockId.Z < CHUNK_TERRAIN_SIZE * AChunk::CHUNK_HEIGHT);

	const FIntPoint ChunkId = { BlockId.X / AChunk::CHUNK_WIDTH, BlockId.Y / AChunk::CHUNK_LENGTH };
	auto Chunk = ActiveChunks.Find(ChunkId);

	if (Chunk == nullptr) {
		UE_LOG(LogTemp, Log, TEXT("No chunk"));
		return false;
	}

	const int32 W = BlockId.X % AChunk::CHUNK_WIDTH;
	const int32 L = BlockId.Y % AChunk::CHUNK_LENGTH;
	const int32 H = BlockId.Z;

	(*Chunk)->SetBlock(W, L, H, Block);

	if (UpdateMesh) {
		(*Chunk)->UpdateMesh();
	}

	return true;
}


// Called every frame
void ATerrain::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UpdateChunks();
	CreateMissingChunks();
}

FVector ATerrain::ChunkIdToLocation(const FIntPoint& ChunkId)
{
	return { ChunkId.X * FSection::SECTION_WORLD_WIDTH - WORLD_CENTER_OFFSET.X,
		ChunkId.Y * FSection::SECTION_WORLD_LENGTH - WORLD_CENTER_OFFSET.Y, -WORLD_CENTER_OFFSET.Z };
}

FIntVector ATerrain::LocationToBlockId(const FVector& Location)
{
	const int32 BlockX = (Location.X + WORLD_CENTER_OFFSET.X) / FBlock::WORLD_SIZE;
	const int32 BlockY = (Location.Y + WORLD_CENTER_OFFSET.Y) / FBlock::WORLD_SIZE;
	const int32 BlockZ = (Location.Z + WORLD_CENTER_OFFSET.Z) / FBlock::WORLD_SIZE;

	return { BlockX, BlockY, BlockZ };
}

TArray<FIntPoint> ATerrain::GetChunkIdsAround(const FIntPoint& CenterChunkId, int32 Width, int32 Length)
{
	TArray<FIntPoint> Chunks;

	const int StartY = FMath::Max(0, CenterChunkId.Y - Length / 2);
	const int EndY = FMath::Min(CHUNK_TERRAIN_SIZE * AChunk::CHUNK_LENGTH - 1, CenterChunkId.Y + Length / 2);

	const int StartX = FMath::Max(0, CenterChunkId.X - Width / 2);
	const int EndX = FMath::Min(CHUNK_TERRAIN_SIZE * AChunk::CHUNK_WIDTH - 1, (CenterChunkId.X + Width / 2));

	for (int Y = StartY; Y <= EndY; ++Y) {
		for (int X = StartX; X <= EndX; ++X) {
			Chunks.Add({X, Y});
		}
	}

	return Chunks;
}

void ATerrain::CreateMissingChunks()
{
	if (MissingChunks.Num() == 0) {
		return;
	}

	const int32 Count = FMath::Min(NumCreateMissingChunksPerTick, MissingChunks.Num());

	for (int32 i = 0; i < Count; ++i) {
		const auto ChunkId = MissingChunks[i];
		NewChunkAt(ChunkId);
	}

	MissingChunks.RemoveAt(0, Count);
}

FIntPoint ATerrain::GetPlayerChunkId() const
{
	auto PlayerController = GetWorld()->GetFirstPlayerController();

	if (PlayerController == nullptr || PlayerController->GetCharacter() == nullptr) {
		return FIntPoint::NoneValue;
	}

	const auto PlayerLocation = PlayerController->GetCharacter()->GetActorLocation();
	return LocationToChunkId(PlayerLocation.X, PlayerLocation.Y);
}

void ATerrain::LoadOrGenerateChunkAsync(AChunk* Chunk)
{
	check(Chunk != nullptr);

	AsyncTask(ENamedThreads::AnyHiPriThreadNormalTask, [Chunk]()
		{
			const auto Filename = GetChunkFilename(Chunk);
			const auto ChunkId = Chunk->GetChunkId();
			TArray<uint8> Blocks;
			Blocks.Reserve(AChunk::CHUNK_RAW_SIZE);

			if (IFileManager::Get().FileExists(*Filename) && FFileHelper::LoadFileToArray(Blocks, *Filename) && Blocks.Num() == AChunk::CHUNK_RAW_SIZE) {
				// copy loaded Raw Block Data to Chunk
				FMemory::Memcpy(Chunk->GetRawData().GetData(), Blocks.GetData(), Blocks.Num());
			} else {
				Chunk->GenerateBlocks();
			}

			// Now schedule the mesh update
			AsyncTask(ENamedThreads::GameThread, [Chunk]()
				{
					Chunk->SetActorLocation(ChunkIdToLocation(Chunk->GetChunkId()));
					Chunk->UpdateMesh();
				});
		});
}

AChunk* ATerrain::NewChunkAt(const FIntPoint& ChunkId)
{
	const bool ReuseOldChunk = OldChunks.Num() > 0;

	auto* Chunk = ReuseOldChunk ? OldChunks[0] : GetWorld()->SpawnActor<AChunk>(FVector(0, 0, -10000), FRotator::ZeroRotator);
	Chunk->SetChunkId(ChunkId);

	if (ReuseOldChunk) {
		OldChunks.RemoveAt(0);
	} else {
		Chunk->SetTerrain(this);
	}

	LoadOrGenerateChunkAsync(Chunk);
	ActiveChunks.Add(ChunkId, Chunk);
	return Chunk;
}

void ATerrain::SaveChunkAsync(AChunk* Chunk)
{
	check(Chunk != nullptr);
	const auto ChunkId = Chunk->GetChunkId();

	const auto Filename = GetChunkFilename(Chunk);
	TArray<uint8> SaveData(Chunk->GetRawData());

	AsyncTask(ENamedThreads::AnyHiPriThreadNormalTask, [Filename, SaveData]()
		{
			FFileHelper::SaveArrayToFile(SaveData, *Filename);
		});
}

void ATerrain::UpdateChunks()
{
	auto PlayerChunkId = GetPlayerChunkId();

	if (PlayerChunkId == FIntPoint::NoneValue) {
		// do nothing when player location is unavailable
		return;
	}

	auto ChunkIdsAroundPlayer = GetChunkIdsAround(PlayerChunkId, WidthChunks, LengthChunks);

	// find which new chunks are needed and which are not needed
	TMap<FIntPoint, AChunk*> NewActiveChunks;
	MissingChunks.Empty();

	for (auto& NeededChunkId : ChunkIdsAroundPlayer) {
		AChunk** ExistingActiveChunk = ActiveChunks.Find(NeededChunkId);

		if (ExistingActiveChunk != nullptr) {
			NewActiveChunks.Add(NeededChunkId, *ExistingActiveChunk);
			ActiveChunks.Remove(NeededChunkId);
		} else {
			MissingChunks.Add(NeededChunkId);
		}
	}

	// remaining items in ActiveChunks are not needed so may be reused
	for (auto& Chunk : ActiveChunks) {
		OldChunks.Add(Chunk.Value);
		SaveChunkAsync(Chunk.Value);
	}

	ActiveChunks = NewActiveChunks;

	// prioritize closest chunks
	MissingChunks.Sort([&](const FIntPoint& A, const FIntPoint& B) { 
		const float DistanceA = (A - PlayerChunkId).SizeSquared();
		const float DistanceB = (B - PlayerChunkId).SizeSquared();
		return DistanceA < DistanceB; });
}

FString ATerrain::GetChunkFilename(AChunk* Chunk)
{
	check(Chunk != nullptr);
	const auto ChunkId = Chunk->GetChunkId();
	return FString::Printf(TEXT("%s%s/%i_%i.chunk"), *FPaths::ProjectSavedDir(), *SAVE_DIRECTORY, ChunkId.X, ChunkId.Y);
}

FIntPoint ATerrain::LocationToChunkId(float X, float Y)
{
	return { static_cast<int32>((X + WORLD_CENTER_OFFSET.X) / FSection::SECTION_WORLD_WIDTH),
			 static_cast<int32>((Y + WORLD_CENTER_OFFSET.Y) / FSection::SECTION_WORLD_LENGTH) };
}
