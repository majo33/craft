#pragma once

#include "CoreMinimal.h"
#include "TerrainHit.h"
#include "GameFramework/Character.h"
#include "CraftCharacter.generated.h"

class UInputComponent;
struct FTerrainHit;

enum class EInventoryItemType {
	None,
	Pickaxe,
	Block
};

struct FInventoryItem {
	EInventoryItemType Type = EInventoryItemType::None;
	uint32 BlockType = 0;
};

UCLASS(config=Game)
class ACraftCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class UStaticMeshComponent* Pickaxe;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	FTerrainHit LastTerrainHit;

	float DiggingStartTime = -1;
	int32 LastHitCount = 0;
	bool bFire = false;

	UPROPERTY(EditAnywhere)
	float MaxDigDistance = 400;
	
	UPROPERTY(EditAnywhere)
	float OneHitDuration = 0.5f;

	// Inventory
	TArray<FInventoryItem> InventoryItems;
	int32 InventoryCurrentItem = -1;

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
	
	ACraftCharacter();

protected:
	void BeginPlay() override;
	
	void OnFireStart();

	void OnFireStop();

	void OnInventoryLeft();

	void OnInventoryRight();

	void OnInventoryNext();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

protected:
	void SetupPlayerInputComponent(UInputComponent* InputComponent) override;

public:
	FInventoryItem GetCurrentInventoryItem() const;

	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	UFUNCTION(BlueprintCallable)
	bool IsFire() const
	{
		return bFire;
	}

	void Tick(float DeltaTime) override;


private:
	void CheckDigging(const FTerrainHit& TerrainHit, float DeltaTime);

	FTerrainHit GetViewPointHit(bool Build = false);

	void InventoryUpdate();

	static void DrawBox(UWorld* World, const FBox& Box, const FColor& Color, uint8 DepthPriority, float Thickness);
};

