#include "CraftHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "CraftCharacter.h"
#include "BlockInfo.h"

ACraftHUD::ACraftHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/Textures/T_Crosshair"));
	CrosshairTex = CrosshairTexObj.Object;
	
	static ConstructorHelpers::FObjectFinder<UTexture2D> BlocksTextureObj(TEXT("/Game/Textures/T_Blocks"));
	BlocksTexture = BlocksTextureObj.Object;
}


void ACraftHUD::DrawHUD()
{
	Super::DrawHUD();

	// draw the crosshair
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);
	const FVector2D CrosshairDrawPosition( (Center.X - CrosshairTex->GetSurfaceWidth() * 0.5f),
										   (Center.Y - CrosshairTex->GetSurfaceHeight() * 0.5f));
	
	FCanvasTileItem CrosshairTileItem( CrosshairDrawPosition, CrosshairTex->GetResource(), FLinearColor::White);
	CrosshairTileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(CrosshairTileItem);

	auto CraftCharacter = Cast<ACraftCharacter>(GetOwningPawn());

	if (CraftCharacter != nullptr) {
		const FVector2D BlockDrawPos(Canvas->ClipY * 0.02f, Canvas->ClipY * 0.86f);

		auto InventoryItem = CraftCharacter->GetCurrentInventoryItem();

		if (InventoryItem.Type == EInventoryItemType::Block) {
			const auto& BlockInfo = BlockInfoTable[InventoryItem.BlockType];

			const FVector2D ItemUV0 = BlockInfo.Faces.Front[0];
			const FVector2D ItemUV1 = BlockInfo.Faces.Front[1];

			FCanvasTileItem BlockTileItem(BlockDrawPos, BlocksTexture->GetResource(), { Canvas->ClipX * 0.06f, Canvas->ClipX * 0.06f }, ItemUV0, ItemUV1, FLinearColor::White);
			BlockTileItem.BlendMode = SE_BLEND_Translucent;
			Canvas->DrawItem(BlockTileItem);
		}
	}
}