#pragma once

#include "CoreMinimal.h"
#include "Section.h"
#include "GameFramework/Actor.h"
#include "Chunk.generated.h"

class ATerrain;
class UProceduralMeshComponent;
struct FBlockInfo;

struct FBlockMesh {
	TArray<FVector> Vertices;
	TArray<FVector> Normals;
	TArray<FVector2D> UV;
	TArray<int32> Triangles;

	void Clear();

	void Reserve();
};

UCLASS()
class CRAFT_API AChunk : public AActor
{
	GENERATED_BODY()

	static const int32 CHUNK_SECTIONS = 4;	///< number of sections in one chunk, e.g. chunk height is 16x4=64 blocks
	static const int32 CHUNK_BLOCK_SIZE = CHUNK_SECTIONS * FSection::SECTION_HEIGHT;
	static const int32 CHUNK_HALF_BLOCK_SIZE = CHUNK_BLOCK_SIZE / 2;

	UPROPERTY()
	UProceduralMeshComponent* PMC;

	UPROPERTY(EditAnywhere)
	UMaterial* Material;

	UPROPERTY(EditAnywhere)
	FIntPoint ChunkId;

	UPROPERTY()
	ATerrain* Terrain;

	FSection Sections[CHUNK_SECTIONS] = {};
	
public:	
	static const int32 CHUNK_WIDTH;
	static const int32 CHUNK_LENGTH;
	static const int32 CHUNK_HEIGHT;
	static const int32 CHUNK_RAW_SIZE;

	AChunk();

	FBlock GetBlock(int32 W, int32 L, int32 H) const;

	FIntPoint GetChunkId() const;

	TArrayView<uint8> GetRawData()
	{
		return TArrayView<uint8>(reinterpret_cast<uint8*>(&Sections[0]), sizeof(Sections));
	}

	ATerrain* GetTerrain() const
	{
		return Terrain;
	}

	/**
	* Change block data, don't forget to call UpdateMesh to apply update visuals
	*/
	void SetBlock(int32 W, int32 L, int32 H, FBlock Block);

#if WITH_EDITOR
	void OnConstruction(const FTransform& Transform) override;
#endif

	void SetChunkId(const FIntPoint& InChunkId)
	{
		ChunkId = InChunkId;
	}

	void SetTerrain(ATerrain* InTerrain)
	{
		Terrain = InTerrain;
	}

	void GenerateBlocks();

	void UpdateMesh();

	void Tick(float DeltaTime) override;

private:
	void SetBlock(uint32 X, uint32 Y, uint32 Z, FBlock Block);

	static void AddBlockMesh(int32 X, int32 Y, int32 Z, const FBlockInfo& BlockInfo, EBlockFace BlockFaces, FBlockMesh& Mesh);

	static const FBlockInfo& GetBlockInfo(uint32 BlockType);
};
