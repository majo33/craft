#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
struct CRAFT_API FBlock
{
	uint32 Type : 3;	// 0 - empty
	uint32 Health : 10;
	uint32 Reserved : 19;

public:
	static const float WORLD_SIZE;	///< size of block in World units (cm)
	static const float HALF_WORLD_SIZE;
};

static_assert(sizeof(FBlock) == 4, "Block size must be 32bits");