#include "Section.h"
#include "BlockInfo.h"

const float FSection::SECTION_WORLD_WIDTH = SECTION_WIDTH * FBlock::WORLD_SIZE;
const float FSection::SECTION_WORLD_LENGTH = SECTION_LENGTH * FBlock::WORLD_SIZE;
const float FSection::SECTION_WORLD_HEIGHT = SECTION_HEIGHT * FBlock::WORLD_SIZE;

const float FSection::SECTION_HALF_WORLD_WIDTH = SECTION_WORLD_WIDTH / 2;
const float FSection::SECTION_HALF_WORLD_LENGTH = SECTION_WORLD_LENGTH / 2;
const float FSection::SECTION_HALF_WORLD_HEIGHT = SECTION_WORLD_HEIGHT / 2;

EBlockFace FSection::GetVisibleFaces(uint32 W, uint32 L, uint32 H) const
{
	uint32 Result = EBlockFace::BF_None;

	// front
	if (L == (SECTION_LENGTH - 1) || IsBlockAtAir(W, L + 1, H)) {
		Result |= EBlockFace::BF_Front;
	}
	// back
	if (L == 0 || IsBlockAtAir(W, L - 1, H)) {
		Result |= EBlockFace::BF_Back;
	}
	// left
	if (W == 0 || IsBlockAtAir(W - 1, L, H)) {
		Result |= EBlockFace::BF_Left;
	}
	// right
	if (W == (SECTION_WIDTH - 1) || IsBlockAtAir(W + 1, L, H)) {
		Result |= EBlockFace::BF_Right;
	}
	// top
	if (H == (SECTION_HEIGHT - 1) || IsBlockAtAir(W, L, H + 1)) {
		Result |= EBlockFace::BF_Top;
	}
	// bottom
	if (H == 0 || IsBlockAtAir(W, L, H - 1)) {
		Result |= EBlockFace::BF_Bottom;
	}

	return static_cast<EBlockFace>(Result);
}

bool FSection::IsBlockAtAir(uint32 W, uint32 L, uint32 H) const
{
	check(W < SECTION_WIDTH);
	check(L < SECTION_LENGTH);
	check(H < SECTION_HEIGHT);
	return Blocks[H][L][W].Type == BLOCK_AIR.Type;
}
