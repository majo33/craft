#pragma once

#include "CoreMinimal.h"
#include "TerrainHit.generated.h"

class AChunk;

USTRUCT()
struct CRAFT_API FTerrainHit {
	GENERATED_BODY()

	UPROPERTY()
	FIntVector BlockId = FIntVector::NoneValue;

	UPROPERTY()
	AChunk* Chunk = nullptr;

	bool IsValid() const
	{
		return BlockId != FIntVector::NoneValue;
	}
};