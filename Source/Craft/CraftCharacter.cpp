#include "CraftCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/LineBatchComponent.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/GameplayStatics.h"
#include "Chunk.h"
#include "Terrain.h"
#include "BlockInfo.h"
#include "TerrainHit.h"

ACraftCharacter::ACraftCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	// Create a Pickaxe mesh
	Pickaxe = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Pickaxe"));
	Pickaxe->SetOnlyOwnerSee(true);
	Pickaxe->bCastDynamicShadow = false;
	Pickaxe->CastShadow = false;
	Pickaxe->SetupAttachment(RootComponent);

	// initialize Inventory
	InventoryItems.Add({EInventoryItemType::Pickaxe});
	InventoryItems.Add({EInventoryItemType::Block, BLOCK_DIRT.Type});
	InventoryItems.Add({EInventoryItemType::Block, BLOCK_GRASS.Type});
	InventoryItems.Add({EInventoryItemType::Block, BLOCK_SNOW.Type});
	InventoryItems.Add({EInventoryItemType::Block, BLOCK_SAND.Type});
	InventoryItems.Add({EInventoryItemType::Block, BLOCK_STONE.Type});
	InventoryCurrentItem = 0;
}

void ACraftCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
	Pickaxe->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("ToolPoint"));
}

//////////////////////////////////////////////////////////////////////////
// Input

void ACraftCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ACraftCharacter::OnFireStart);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ACraftCharacter::OnFireStop);

	PlayerInputComponent->BindAction("InventoryLeft", IE_Pressed, this, &ACraftCharacter::OnInventoryLeft);
	PlayerInputComponent->BindAction("InventoryRight", IE_Pressed, this, &ACraftCharacter::OnInventoryRight);
	PlayerInputComponent->BindAction("InventoryNext", IE_Pressed, this, &ACraftCharacter::OnInventoryNext);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ACraftCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACraftCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ACraftCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ACraftCharacter::LookUpAtRate);
}

FInventoryItem ACraftCharacter::GetCurrentInventoryItem() const
{
	if (InventoryItems.Num() > 0) {
		return InventoryItems[InventoryCurrentItem];
	} else {
		return { EInventoryItemType::None };
	}
}

void ACraftCharacter::OnFireStart()
{
	bFire = true;
	LastHitCount = 0;
	DiggingStartTime = GetWorld()->GetTimeSeconds();
	auto InventoryItem = GetCurrentInventoryItem();

	if (InventoryItem.Type == EInventoryItemType::Block) {
		const auto TerrainHit = GetViewPointHit(true);

		if (TerrainHit.IsValid()) {
			check(TerrainHit.Chunk != nullptr);
			TerrainHit.Chunk->GetTerrain()->SetBlockAt(TerrainHit.BlockId, MakeBlock(InventoryItem.BlockType));
		}
	}
}

void ACraftCharacter::OnFireStop()
{
	bFire = false;
	LastHitCount = 0;
	DiggingStartTime = 0;
}

void ACraftCharacter::OnInventoryLeft()
{
	InventoryCurrentItem = FMath::Max(InventoryCurrentItem - 1, 0);
	InventoryUpdate();
}

void ACraftCharacter::OnInventoryRight()
{
	InventoryCurrentItem = FMath::Min(InventoryCurrentItem + 1, 0);
	InventoryUpdate();
}

void ACraftCharacter::OnInventoryNext()
{
	InventoryCurrentItem = (InventoryCurrentItem + 1) % InventoryItems.Num();
	InventoryUpdate();
}

void ACraftCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ACraftCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ACraftCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ACraftCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ACraftCharacter::Tick(float DeltaTime)
{
	const auto InventoryItem = GetCurrentInventoryItem();

	if (InventoryItem.Type == EInventoryItemType::None)	{
		return;
	}

	const auto BuildMode = InventoryItem.Type == EInventoryItemType::Block;
	auto TerrainHit = GetViewPointHit(BuildMode);

	if (TerrainHit.IsValid()) {
		auto BoxMin = FVector(TerrainHit.BlockId.X * FBlock::WORLD_SIZE,
			TerrainHit.BlockId.Y * FBlock::WORLD_SIZE,
			TerrainHit.BlockId.Z * FBlock::WORLD_SIZE) - ATerrain::WORLD_CENTER_OFFSET;
		auto BoxMax = BoxMin + FVector(FBlock::WORLD_SIZE, FBlock::WORLD_SIZE, FBlock::WORLD_SIZE);
		DrawBox(GetWorld(), FBox(BoxMin, BoxMax), BuildMode ? FColor::Yellow : FColor::Red, 1, 4);
	}

	if (InventoryItem.Type == EInventoryItemType::Pickaxe && bFire) {
		CheckDigging(TerrainHit, DeltaTime);
	}

	LastTerrainHit = TerrainHit;
}

void ACraftCharacter::CheckDigging(const FTerrainHit& TerrainHit, float DeltaTime)
{
	if (TerrainHit.IsValid() && TerrainHit.BlockId == LastTerrainHit.BlockId) {
		check(TerrainHit.Chunk->GetTerrain() != nullptr);

		const auto DiggingDuration = GetWorld()->GetTimeSeconds() - DiggingStartTime;
		const auto CurrentHit = int32(DiggingDuration / OneHitDuration);

		if (CurrentHit != LastHitCount) {
			LastHitCount = CurrentHit;
			auto Terrain = TerrainHit.Chunk->GetTerrain();

			auto Block = Terrain->GetBlockAt(TerrainHit.BlockId);
			Block.Health -= 1;

			if (Block.Health == 0) {
				Terrain->SetBlockAt(TerrainHit.BlockId, BLOCK_AIR, true);
			} else {
				Terrain->SetBlockAt(TerrainHit.BlockId, Block, false);
			}
		}
	} else {
		LastHitCount = 0;
		DiggingStartTime = GetWorld()->GetTimeSeconds();
	}
}

FTerrainHit ACraftCharacter::GetViewPointHit(bool Build)
{
	check(FirstPersonCameraComponent != nullptr);

	FMinimalViewInfo CameraView;
	FirstPersonCameraComponent->GetCameraView(0, CameraView);

	FHitResult Hit;
	FVector Start = CameraView.Location;
	FVector End = Start + CameraView.Rotation.RotateVector(FVector::ForwardVector) * MaxDigDistance;
	auto QueryParams = FCollisionQueryParams::DefaultQueryParam;
	QueryParams.AddIgnoredActor(this);

	if (!GetWorld()->LineTraceSingleByChannel(Hit, CameraView.Location, End, ECC_WorldStatic, QueryParams)) {
		return {};
	}

	auto Chunk = Cast<AChunk>(Hit.GetActor());

	if (Chunk == nullptr) {
		return {};
	}

	auto WorldLocation = Hit.Location + (Build ? Hit.Normal : -Hit.Normal);
	return { ATerrain::LocationToBlockId(WorldLocation), Chunk };
}

void ACraftCharacter::InventoryUpdate()
{
	if (InventoryItems.Num() == 0) {
		return;
	}

	Pickaxe->SetHiddenInGame(GetCurrentInventoryItem().Type != EInventoryItemType::Pickaxe);
}

void ACraftCharacter::DrawBox(UWorld* World, const FBox& Box, const FColor& Color, uint8 DepthPriority, float Thickness)
{
	// top face
	World->LineBatcher->DrawLine({ Box.Min.X, Box.Min.Y, Box.Max.Z }, { Box.Max.X, Box.Min.Y, Box.Max.Z }, Color, DepthPriority, Thickness);
	World->LineBatcher->DrawLine({ Box.Min.X, Box.Max.Y, Box.Max.Z }, { Box.Max.X, Box.Max.Y, Box.Max.Z }, Color, DepthPriority, Thickness);
	World->LineBatcher->DrawLine({ Box.Min.X, Box.Min.Y, Box.Max.Z }, { Box.Min.X, Box.Max.Y, Box.Max.Z }, Color, DepthPriority, Thickness);
	World->LineBatcher->DrawLine({ Box.Max.X, Box.Min.Y, Box.Max.Z }, { Box.Max.X, Box.Max.Y, Box.Max.Z }, Color, DepthPriority, Thickness);

	// bottom face
	World->LineBatcher->DrawLine({ Box.Min.X, Box.Min.Y, Box.Min.Z }, { Box.Max.X, Box.Min.Y, Box.Min.Z }, Color, DepthPriority, Thickness);
	World->LineBatcher->DrawLine({ Box.Min.X, Box.Max.Y, Box.Min.Z }, { Box.Max.X, Box.Max.Y, Box.Min.Z }, Color, DepthPriority, Thickness);
	World->LineBatcher->DrawLine({ Box.Min.X, Box.Min.Y, Box.Min.Z }, { Box.Min.X, Box.Max.Y, Box.Min.Z }, Color, DepthPriority, Thickness);
	World->LineBatcher->DrawLine({ Box.Max.X, Box.Min.Y, Box.Min.Z }, { Box.Max.X, Box.Max.Y, Box.Min.Z }, Color, DepthPriority, Thickness);

	// vertical connecting lines
	World->LineBatcher->DrawLine({ Box.Min.X, Box.Min.Y, Box.Min.Z }, { Box.Min.X, Box.Min.Y, Box.Max.Z }, Color, DepthPriority, Thickness);
	World->LineBatcher->DrawLine({ Box.Max.X, Box.Min.Y, Box.Min.Z }, { Box.Max.X, Box.Min.Y, Box.Max.Z }, Color, DepthPriority, Thickness);
	World->LineBatcher->DrawLine({ Box.Min.X, Box.Max.Y, Box.Min.Z }, { Box.Min.X, Box.Max.Y, Box.Max.Z }, Color, DepthPriority, Thickness);
	World->LineBatcher->DrawLine({ Box.Max.X, Box.Max.Y, Box.Min.Z }, { Box.Max.X, Box.Max.Y, Box.Max.Z }, Color, DepthPriority, Thickness);
}
