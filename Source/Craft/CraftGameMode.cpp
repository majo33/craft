#include "CraftGameMode.h"
#include "CraftHUD.h"
#include "CraftCharacter.h"
#include "UObject/ConstructorHelpers.h"

ACraftGameMode::ACraftGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Character/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;
	PlayerControllerClass = APlayerController::StaticClass();

	// use our custom HUD class
	HUDClass = ACraftHUD::StaticClass();
}
