# Controls 

## Keyboard

* A,W,S,D - movement
* Q,E - change item/tool
* Space - jump
* arrows - turn, movement

## Mouse

* Primary - build/dig
* Secondary - change item/tool

## Gamepad (X360)

* Left Stick - movement
* Right Stick - view
* Right Trigger - build/dig
* Left/Right Shoulder - change item/tool
* X - jump

# Notes

* created in Unreal Engine 4.25.4
* terrain is saved during gameplay into $User$/AppData/Local/Craft/Saved/Save
* to reset game state delete $User$/AppData/Local/Craft/Saved/Save
* on Linux add 5.1 version .config/Epic/UnrealEngine/Install.ini:
  [Installations]
  {8E3E0FA1-29C2-41C0-B0F6-D0120713A214}=/home/majo33/dev/UnrealEngine
  5.1=/home/majo33/dev/UnrealEngine
